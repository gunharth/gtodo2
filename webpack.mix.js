let mix = require("laravel-mix");

mix.js("src/app.js", "public/main.js");
mix.sass("src/styles.scss", "public");
