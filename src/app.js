import $ from "jquery";

// MVC
// Model = Daten / Datenstruktur
// View = Anzeige
// Controller = Logic des Programms

//$(document).ready(function () {
$(function () {

  var firebaseConfig = {
    apiKey: "AIzaSyAgI2z-Nt9zp0wUposaGfkbQ3nkkHG1njg",
    authDomain: "bfi-todos.firebaseapp.com",
    projectId: "bfi-todos",
    storageBucket: "bfi-todos.appspot.com",
    messagingSenderId: "285940948270",
    appId: "1:285940948270:web:842ad7c089a4bb04bceebf",
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  var db = firebase.firestore();

  //let todosArray = getLocalStorage();
  let todosArray = [];

  db.collection("todos").onSnapshot((querySnapshot) => {
    todosArray = [];
    querySnapshot.forEach((doc) => {
      let todo = {
        id: doc.id,
        name: doc.data().name,
        erledigt: doc.data().erledigt,
      };
      todosArray.push( todo );
      //console.log(doc.id, doc.data().name, doc.data().erledigt);
    });
    render();
  });

  let todos = $("#todos"); // die todos ul liste
  let input = $("#todo-input");
  let button = $("#todo-button");

  // events
  //button.click()
  button.on('click', function (event) {
    addTodo();
  });

  $(document).on('keyup', function (event) {
    if (event.code === 13) addTodo();
  });

  // functions
  function addTodo() {
    if (input.val() != "") {
      db.collection("todos").add({
        name: input.val(),
        erledigt: false,
      });
      input.val("");
    }
  }

  function render() {
    todos.html("");
    for (let i = 0; i < todosArray.length; i++) {
      let style = "";
      if (i == todosArray.length - 1) style = "display: none";
      let li = $(
        '<li style="' +
          style +
          '">' +
          //todosArray[i].id + ' ' +
          todosArray[i].name + ' ' +
          todosArray[i].erledigt +
          "</li>"
      );
      todos.append(li);
      li.fadeIn();
    }
  }
  render();

    // function getLocalStorage() {
    //         // ternary operator
    //   return localStorage.getItem("Model") ? JSON.parse(localStorage.getItem("Model")) : [];
    // }
    function setLocalStorage() {
      localStorage.setItem("Model", JSON.stringify(todosArray));
    }
});
